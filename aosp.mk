## Specify phone tech before including full_phone
$(call inherit-product, vendor/aosplus/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := shamu

# Boot animation
TARGET_SCREEN_HEIGHT := 2560
TARGET_SCREEN_WIDTH := 1440

# Inherit some common AOSPlus stuff.
$(call inherit-product, vendor/aosplus/config/common_full_phone.mk)

# Enhanced NFC
$(call inherit-product, vendor/aosplus/config/nfc_enhanced.mk)

# Inherit device configuration
$(call inherit-product, device/moto/shamu/aosp_shamu.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := shamu
PRODUCT_NAME := aosp_shamu
PRODUCT_BRAND := Google
PRODUCT_MODEL := Nexus 6 Shamu
PRODUCT_MANUFACTURER := motorola
PRODUCT_RESTRICT_VENDOR_FILES := false

